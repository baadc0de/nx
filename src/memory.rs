use std::ptr::addr_of;

use rand::random;

use crate::types::TState;

pub trait Contended {
    fn contend(&self, address: u16, delay: TState, num: u32, t: &mut TState);
}

pub trait ReadableMemory: Contended {
    fn quick_peek(&self, address: u16) -> u8;
    fn peek(&self, address: u16, t: &mut TState) -> u8;
}

pub trait WritableMemory: Contended {
    fn poke(&mut self, address: u16, b: u8, t: &mut TState);
}

pub trait ReadableMemory16<T: ReadableMemory> {
    fn peek16(&self, address: u16, t: &mut TState) -> u16;
}

pub trait WritableMemory16<T: WritableMemory> {
    fn poke16(&mut self, address: u16, w: u16, t: &mut TState);
}

impl<T: ReadableMemory> ReadableMemory16<T> for T {
    fn peek16(&self, address: u16, t: &mut TState) -> u16 {
        let l = self.peek(address, t);
        let h = self.peek(address + 1, t);
        ((h as u16) * 256 + (l as u16))
    }
}
impl<T: WritableMemory> WritableMemory16<T> for T {
    fn poke16(&mut self, address: u16, w: u16, t: &mut TState) {
        let h = (w >> 8) as u8;
        let l = w as u8;
        self.poke(address, l, t);
        self.poke(address + 1, h, t);
    }
}

pub struct Memory48 {
    memory: Vec<u8>,
    contention: Vec<TState>,
    rom_writable: bool,
}

impl Memory48 {
    pub fn new() -> Self {
        let mut memory: Vec<u8> = (0..65536).map(|_| random()).collect();
        let mut contention = vec![0; 70930];

        let contention_start = 14335usize;
        let contention_end = contention_start + (192 * 224);
        let mut t = contention_start;
        while t < contention_end {
            // Calculate contention for the next 128 t-states (i.e. a single pixel line)
            for i in (0..128).skip(8) {
                contention[t + 0] = 6;
                contention[t + 1] = 5;
                contention[t + 2] = 4;
                contention[t + 3] = 3;
                contention[t + 4] = 2;
                contention[t + 5] = 1;
                contention[t + 6] = 0;
                contention[t + 7] = 0;
                t += 8;
            }

            // Skip the time the border is being drawn: 96 t-states (24 left, 24 right, 48 retrace).
            t += (224 - 128);
        }

        Self {
            memory,
            contention,
            rom_writable: false,
        }
    }

    pub fn load_screen(&mut self, bytes: &[u8; 6912]) {
        let mut i = 0;
        for addr in (0x4000u16..(0x4000 + 6912)) {
            self.memory[addr as usize] = bytes[i];
            i += 1;
        }
    }
}

impl Contended for Memory48 {
    fn contend(&self, address: u16, delay: TState, num: u32, t: &mut TState) {
        if (address & 0xc000) == 0x4000 {
            for i in (0..num) {
                *t += self.contention[*t as usize] + delay;
            }
        } else {
            *t += delay * num;
        }
    }
}

impl ReadableMemory for Memory48 {
    fn quick_peek(&self, address: u16) -> u8 {
        self.memory[address as usize]
    }

    fn peek(&self, address: u16, t: &mut TState) -> u8 {
        self.contend(address, 3, 1, t);
        self.memory[address as usize]
    }
}

impl WritableMemory for Memory48 {
    fn poke(&mut self, address: u16, b: u8, t: &mut TState) {
        if self.rom_writable || address >= 16384 {
            self.memory[address as usize] = b;
        }
        self.contend(address, 3, 1, t);
    }
}
