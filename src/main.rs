#![allow(unused)]

mod emulator;
mod memory;
mod types;

use emulator::Emulator;
use memory::{ReadableMemory, WritableMemory};
use pixels::{Pixels, SurfaceTexture};
use pixels_u32::PixelsExt;
use winit::{
    dpi::PhysicalSize,
    event::{Event, VirtualKeyCode},
    event_loop::{ControlFlow, EventLoop},
    window::WindowBuilder,
};
use winit_input_helper::WinitInputHelper;

//
// Version Constants
//

const NX_MAJOR_VERSION: i32 = 0;
const NX_MINOR_VERSION: i32 = 0;
const NX_PATCH_VERSION: i32 = 8;
const NX_DEV_MINOR: i32 = 0;
const NX_DEV_PATCH: char = 'a';

fn version_string() -> String {
    if NX_MAJOR_VERSION == 0 {
        if NX_MINOR_VERSION == 0 {
            format!("Dev.{NX_PATCH_VERSION}.{NX_DEV_MINOR}{NX_DEV_PATCH}")
        } else {
            if NX_MINOR_VERSION == 9 {
                format!("Beta.{NX_PATCH_VERSION}")
            } else {
                format!("Alpha.{NX_MINOR_VERSION}.{NX_PATCH_VERSION}")
            }
        }
    } else {
        format!("{NX_MAJOR_VERSION}.{NX_MINOR_VERSION}.{NX_PATCH_VERSION}")
    }
}

//
// Emulator constants
//

/// Size of the actual pixel area of the screen
const SCREEN_SIZE: (u32, u32) = (256, 192);

/// Size of the TV that encompasses video image.
///
/// Each line has 48 pixels of border, 256 pixels of pixel data, followed by
/// another 48 pixels of border.  The screen comprises of 64 lines of border,
/// 192 lines of pixel data, followed by 56 lines of border.
const TV_SIZE: (u32, u32) = (352, 312);

/// The size of the window that displays the emulated image.  It can be smaller
/// than the TV size.
const WINDOW_SIZE: (u32, u32) = (320, 256);

/// Size of the borders around the video area inside the window.
const BORDER_SIZE: (u32, u32) = (
    (WINDOW_SIZE.0 - SCREEN_SIZE.0) / 2,
    (WINDOW_SIZE.1 - SCREEN_SIZE.1) / 2,
);

/// Size of the UI area that is overlayed over the screen.  It is always twice
/// the resolution.
const UI_SIZE: (u32, u32) = (WINDOW_SIZE.0 * 2, WINDOW_SIZE.1 * 2);

/// Default scale (number of real pixels per pixel) of window.
const DEFAULT_SCALE: u32 = 4;

//
// Main loop
//

fn main() {
    let event_loop = EventLoop::new();
    let window = WindowBuilder::new()
        .with_title(format!("NX Nova ({})", version_string()))
        .with_inner_size(PhysicalSize::new(
            WINDOW_SIZE.0 * DEFAULT_SCALE,
            WINDOW_SIZE.1 * DEFAULT_SCALE,
        ))
        .with_resizable(false)
        .build(&event_loop)
        .expect("Unable to open window");
    let mut pixels = {
        let window_size = window.inner_size();
        let surface_texture = SurfaceTexture::new(window_size.width, window_size.height, &window);
        Pixels::new(WINDOW_SIZE.0, WINDOW_SIZE.1, surface_texture)
    }
    .expect("Unable to set up the pixels display");
    let mut input = WinitInputHelper::new();
    let mut emulator = Emulator::new();

    emulator
        .memory
        .load_screen(include_bytes!("../data/aticatac.scr"));

    event_loop.run(move |event, _, control_flow| {
        //
        // Render the screen
        //
        if let Event::RedrawRequested(_) = event {
            draw(pixels.get_frame_u32(), &mut emulator);
            if pixels.render().is_err() {
                *control_flow = ControlFlow::Exit;
                return;
            }
        }

        //
        // Handle input
        //
        if input.update(&event) {
            // Close events
            if input.key_pressed(VirtualKeyCode::Escape) || input.quit() {
                *control_flow = ControlFlow::Exit;
                return;
            }
        }
    });
}

fn draw(pixels: &mut [u32], emulator: &mut Emulator) {
    emulator.render(pixels);
}
