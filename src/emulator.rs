use crate::{
    memory::{Memory48, ReadableMemory},
    types::TState,
    BORDER_SIZE, SCREEN_SIZE, TV_SIZE, WINDOW_SIZE,
};

pub struct Emulator {
    pub memory: Memory48,

    /// Current t-state counter within the frame.
    t_states: TState,

    /// T-state of top-left of window.  Anything before this during the frame
    /// doesn't render anything visible.
    start_video_tstate: TState,

    /// T-state of bottom-right of window.
    end_video_tstate: TState,

    /// Current t-state that has been drawn to.
    ///
    /// When updating the video, we draw the pixels from this t-state counter to
    /// the current one.
    draw_tstate: TState,

    /// Index into render texture that we've draw to.
    draw_point: usize,

    video_map: Vec<u16>,
}

impl Emulator {
    pub fn new() -> Self {
        // Start of display area is 14336.  We wait 4 t-states before we
        // draw 8 pixels.  The left border is 24 pixels wide.  Each scan line is 224 t-states long.
        let start_video_tstate = (14340 - 24) - (224 * BORDER_SIZE.1);
        let end_video_tstate = 69888u32;

        // Initialise the t-state -> address map with values:
        //
        //  0xffff   Do not draw
        //  0xfffe   Border colour
        //  0x0000+  Pixel address
        const DO_NOT_DRAW: u16 = 0xffff;
        const DRAW_BORDER: u16 = 0xfffe;

        let mut video_map = vec![DO_NOT_DRAW; end_video_tstate as usize];
        let mut t = start_video_tstate as usize;

        // Calculate line timings
        //
        // +---------- TV width ------------------+
        // |   +------ Window width ----------+   |
        // |   |  +--- Screen width -------+  |   |
        // V   V  V                        V  V   V
        // +---+--+------------------------+--+---+-----+
        // |000|11|aaaaaaaaaaaaaaaaaaaaaaaa|11|000|00000|
        // +---+--+------------------------+--+---+-----+
        //     ta tb                          176-ta    224
        //
        let ta = ((TV_SIZE.0 - WINDOW_SIZE.0) / 4) as usize;
        let tb = ((TV_SIZE.0 - SCREEN_SIZE.0) / 4) as usize;

        // Top border
        while t < (start_video_tstate + (BORDER_SIZE.1 * 224)) as usize {
            for i in 0..ta {
                video_map[t] = DO_NOT_DRAW;
                t += 1;
            }
            for i in ta..(176 - ta) {
                video_map[t] = DRAW_BORDER;
                t += 1;
            }
            for i in (176 - ta)..224 {
                video_map[t] = DO_NOT_DRAW;
                t += 1;
            }
        }

        // Build middle of display
        let mut x = 0u16;
        let mut y = 0u16;
        let mut addr = 2;

        while t < (start_video_tstate + (BORDER_SIZE.1 * 224) + (SCREEN_SIZE.1 * 224)) as usize {
            // Left border
            for i in 0..ta {
                video_map[t] = DO_NOT_DRAW;
                t += 1;
            }
            for i in ta..tb {
                video_map[t] = DRAW_BORDER;
                t += 1;
            }

            // Screen line
            for i in tb..(tb + 128) {
                // Every 4 t-states (8 pixels), we recalculate the address
                if (i % 4 == 0) {
                    // Pixel offset is 000S SRRR CCCX XXXX, where Y = SSCCCRRR
                    // Attr offset is  0001 10YY YYYX XXXX,
                    addr = ((((y & 0xc0) >> 3) | (y & 0x07)) << 8)
                        | (((x >> 3) & 0x1f) | ((y & 0x38) << 2));
                    x += 8;
                }
                video_map[t] = addr;
                t += 1;
            }
            y += 1;

            // Right border
            for i in (tb + 128)..(176 - ta) {
                video_map[t] = DRAW_BORDER;
                t += 1;
            }

            // Horizontal retrace + out of screen border
            for i in (176 - ta)..224 {
                video_map[t] = DO_NOT_DRAW;
                t += 1;
            }
        }

        // Bottom border
        while t
            < (start_video_tstate
                + (BORDER_SIZE.1 * 224)
                + (SCREEN_SIZE.1 * 224)
                + (BORDER_SIZE.1 * 224)) as usize
        {
            for i in 0..ta {
                video_map[t] = DO_NOT_DRAW;
                t += 1;
            }
            for i in ta..(176 - ta) {
                video_map[t] = DRAW_BORDER;
                t += 1;
            }
            for i in (176 - ta)..224 {
                video_map[t] = DO_NOT_DRAW;
                t += 1;
            }
        }

        Self {
            memory: Memory48::new(),
            t_states: 69888,
            start_video_tstate,
            end_video_tstate,
            draw_tstate: 0,
            draw_point: 0,
            video_map,
        }
    }

    pub fn render(&mut self, pixels: &mut [u32]) {
        const COLOURS: [u32; 16] = [
            0xff000000, 0xffd70000, 0xff0000d7, 0xffd700d7, 0xff00d700, 0xffd7d700, 0xff00d7d7,
            0xffd7d7d7, 0xff000000, 0xffff0000, 0xff0000ff, 0xffff00ff, 0xff00ff00, 0xffffff00,
            0xff00ffff, 0xffffffff,
        ];

        let mut t = self.t_states;
        if t >= self.start_video_tstate {
            if t >= self.end_video_tstate {
                t = self.end_video_tstate - 1;
            }
        }

        // It takes 4 t-states to write 1 byte.
        let elapsed_tstates = t + 1 - self.draw_tstate;
        let num_bytes = (elapsed_tstates >> 2) + (if (elapsed_tstates % 4) > 0 { 1 } else { 0 });

        for i in 0..num_bytes {
            let paddr = self.video_map[self.draw_tstate as usize];
            if paddr < 0xfffe {
                let paddr = 0x4000 + paddr;
                let mut pixel_data = self.memory.quick_peek(paddr);
                let aaddr = ((paddr & 0x1800) >> 3) + (paddr & 0x00ff) + 0x5800;
                let attr = self.memory.quick_peek(aaddr);

                let bright = (attr & 0x40) >> 3;
                let ink = attr & 0x07;
                let paper = (attr & 0x38) >> 3;

                let c0 = COLOURS[(paper + bright) as usize];
                let c1 = COLOURS[(ink + bright) as usize];

                // TODO: Deal with flash

                for p in 0..8 {
                    if (pixel_data & 0x80) != 0 {
                        pixels[self.draw_point] = c1;
                        self.draw_point += 1;
                    } else {
                        pixels[self.draw_point] = c0;
                        self.draw_point += 1;
                    }
                    pixel_data <<= 1;
                }
            } else if paddr == 0xfffe {
                let border = COLOURS[0];
                for b in 0..8 {
                    pixels[self.draw_point] = border;
                    self.draw_point += 1;
                }
            }

            self.draw_tstate += 4;
        }

        if self.t_states >= self.end_video_tstate {
            self.draw_point = 0;
            self.draw_tstate = self.start_video_tstate;
        }
    }
}
